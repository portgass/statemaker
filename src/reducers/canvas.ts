import Position, { zoom } from '../interfaces/Position'
import IState from '../interfaces/State'
import ITransition from '../interfaces/Transition'
import { ActionsUnion, createAction } from '../utils/redux'
import { round as roundNum } from '../utils/number'

const WIDTH = document.documentElement.clientWidth
const HEIGHT = document.documentElement.clientHeight

export const Actions = {
	/** Sets cursor to position */
	setCursor: (value: Position) => createAction('setCursor', value),
	/** Sets offset to position */
	setOffset: (value: Position) => createAction('setOffset', value),
	/** Sets cursor mode to select or move */
	setCursorMode: (value: 'select' | 'move') =>
		createAction('setCursorMode', value),
	/** Sets id of state being positioned */
	setPositioningState: (id: string) =>
		createAction('setPositioningState', id),
	/** Sets scale and offset of new zoom factor */
	zoom: (factor: number, to?: Position) =>
		createAction('zoom', { factor, to }),
	/** Deselects canvas elements */
	deselect: () => createAction('deselect'),
	/** Updates values of ghost state */
	updateGhostState: (state: Partial<IState>) =>
		createAction('updateGhostState', state),
	/** Updates values of ghost transition */
	updateGhostTransition: (transition: Partial<ITransition>) =>
		createAction('updateGhostTransition', transition),
	/** Selects state by its id */
	selectState: (id: string) => createAction('selectState', id),
	/** Selects transition by its id */
	selectTransition: (id: string) => createAction('selectTransition', id),
	/** Increases state name id by 1 */
	useStateNameId: () => createAction('useStateNameId'),
	/** Increases transition name id by 1 */
	useTransitionNameId: () => createAction('useTransitionNameId')
}

export type Actions = ActionsUnion<typeof Actions>

export interface State {
	/** Canvas modes of mouse interaction */
	cursorMode: 'select' | 'move'
	/** Cursor position */
	cursor: Position
	/** Canvas offset position */
	offset: Position
	/** Canvas scale */
	scale: number
	/** Ghost state used when building state */
	ghostState: IState
	/** Ghost transition used when building transition */
	ghostTransition: ITransition
	/** Id of selected state */
	selectedState: string
	/** Id of selected transition */
	selectedTransition: string
	/** Id of positioning state */
	positioningState: string
	/** Number of next auto state name number */
	stateNameId: number
	/** Number of next auto transition name number */
	transitionNameId: number
}

export const initialState: State = {
	cursor: { x: 0, y: 0 },
	offset: { x: 0, y: 0 },
	cursorMode: 'select',
	scale: 1,
	ghostState: {
		id: 'state',
		name: 'State',
		position: {
			x: 0,
			y: 0
		},
		size: 29
	},
	ghostTransition: {
		id: 'transition',
		name: 'Transition',
		startState: '',
		endState: ''
	},
	selectedState: '',
	selectedTransition: '',
	positioningState: '',
	stateNameId: 1,
	transitionNameId: 1
}

function reducer(state = initialState, action: Actions): State {
	switch (action.type) {
		case 'setCursor': {
			return {
				...state,
				cursor: action.payload
			}
		}
		case 'setOffset': {
			return {
				...state,
				offset: { ...action.payload }
			}
		}
		case 'setCursorMode': {
			return {
				...state,
				cursorMode: action.payload
			}
		}
		case 'setPositioningState': {
			return {
				...state,
				positioningState: action.payload
			}
		}
		case 'zoom': {
			const newScale = state.scale + action.payload.factor
			if (newScale > 3 || newScale < 0.15) return state
			const to = action.payload.to || { x: WIDTH / 2, y: HEIGHT / 2 }

			const newOffset = zoom(
				to,
				state.offset,
				state.scale,
				action.payload.factor
			)

			return {
				...state,
				scale: roundNum(newScale),
				offset: { ...newOffset }
			}
		}
		case 'deselect': {
			return {
				...state,
				selectedState: '',
				selectedTransition: '',
				ghostTransition: {
					...state.ghostTransition,
					startState: '',
					endState: ''
				}
			}
		}
		case 'updateGhostState': {
			return {
				...state,
				ghostState: {
					...state.ghostState,
					...action.payload
				}
			}
		}
		case 'updateGhostTransition': {
			return {
				...state,
				ghostTransition: {
					...state.ghostTransition,
					...action.payload
				}
			}
		}
		case 'selectState': {
			return {
				...state,
				selectedState:
					state.selectedState === action.payload
						? ''
						: action.payload,
				selectedTransition: ''
			}
		}
		case 'selectTransition': {
			return {
				...state,
				selectedState: '',
				selectedTransition:
					state.selectedTransition === action.payload
						? ''
						: action.payload
			}
		}
		case 'useStateNameId': {
			return {
				...state,
				stateNameId: state.stateNameId + 1
			}
		}
		case 'useTransitionNameId': {
			return {
				...state,
				transitionNameId: state.transitionNameId + 1
			}
		}
		default:
			return state
	}
}

export default reducer
