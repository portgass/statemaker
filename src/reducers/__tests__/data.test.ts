import reducer, { initialState, Actions, State } from '../data'
import force from '../../utils/positioning/force'
import layered from '../../utils/positioning/layered'

describe('data reducer', () => {
	it('adds state', () => {
		jest.mock('uuid')
		expect(
			reducer(
				initialState,
				Actions.addState({
					name: 'x',
					size: 25,
					position: { x: 0, y: 0 }
				})
			)
		).toEqual({
			...initialState,
			states: {
				...initialState.states,
				3: {
					id: '3',
					name: 'x',
					size: 25,
					position: { x: 0, y: 0 }
				}
			}
		} as State)
		jest.resetModules()
	})

	it('adds initial state', () => {
		jest.mock('uuid')
		expect(
			reducer(
				initialState,
				Actions.addState(
					{
						name: 'x',
						size: 25,
						position: { x: 0, y: 0 }
					},
					'initial'
				)
			)
		).toEqual({
			...initialState,
			initialStates: ['4'],
			states: {
				...initialState.states,
				4: {
					id: '4',
					name: 'x',
					size: 25,
					position: { x: 0, y: 0 }
				}
			}
		} as State)
		jest.resetModules()
	})

	it('adds final state', () => {
		expect(
			reducer(
				initialState,
				Actions.addState(
					{
						name: 'x',
						size: 25,
						position: { x: 0, y: 0 }
					},
					'final'
				)
			)
		).toEqual({
			...initialState,
			finalStates: ['5'],
			states: {
				...initialState.states,
				5: {
					id: '5',
					name: 'x',
					size: 25,
					position: { x: 0, y: 0 }
				}
			}
		} as State)
	})

	it('adds transition', () => {
		expect(
			reducer(
				initialState,
				Actions.addTransition({
					name: 'x',
					startState: 'x',
					endState: 'y'
				})
			)
		).toEqual({
			...initialState,
			transitions: {
				...initialState.transitions,
				6: {
					id: '6',
					name: 'x',
					startState: 'x',
					endState: 'y'
				}
			}
		} as State)
	})

	it('updates state', () => {
		expect(
			reducer(
				{
					...initialState,
					states: {
						...initialState.states,
						1: {
							id: '1',
							name: 'x',
							size: 25,
							position: { x: 0, y: 0 }
						}
					}
				},
				Actions.updateState('1', {
					name: 'y'
				})
			)
		).toEqual({
			...initialState,
			states: {
				...initialState.states,
				1: {
					id: '1',
					name: 'y',
					size: 25,
					position: { x: 0, y: 0 }
				}
			}
		} as State)
	})

	it('updates state wrong id', () => {
		expect(
			reducer(initialState, Actions.updateState('1', { name: 'z' }))
		).toEqual(initialState)
	})

	it('updates transition', () => {
		expect(
			reducer(
				{
					...initialState,
					transitions: {
						...initialState.transitions,
						1: {
							id: '1',
							name: 'x',
							startState: 's1',
							endState: 's2'
						}
					}
				},
				Actions.updateTransition('1', {
					name: 'y'
				})
			)
		).toEqual({
			...initialState,
			transitions: {
				...initialState.transitions,
				1: {
					id: '1',
					name: 'y',
					startState: 's1',
					endState: 's2'
				}
			}
		} as State)
	})

	it('updates transition wrong id', () => {
		expect(
			reducer(initialState, Actions.updateTransition('1', { name: 'z' }))
		).toEqual(initialState)
	})

	it('removes state', () => {
		expect(
			reducer(
				{
					...initialState,
					initialStates: ['1'],
					finalStates: ['1'],
					states: {
						...initialState.states,
						1: {
							id: '1',
							name: 'x',
							size: 25,
							position: { x: 0, y: 0 }
						}
					},
					transitions: {
						...initialState.transitions,
						2: {
							id: '2',
							name: 'y',
							startState: '1',
							endState: '1'
						},
						3: {
							id: '3',
							name: 'z',
							startState: '2',
							endState: '2'
						}
					}
				},
				Actions.removeState('1')
			)
		).toEqual({
			...initialState,
			initialStates: [],
			finalStates: [],
			states: {},
			transitions: {
				3: {
					id: '3',
					name: 'z',
					startState: '2',
					endState: '2'
				}
			}
		} as State)
	})

	it('removes state wrong id', () => {
		expect(reducer(initialState, Actions.removeState('1'))).toEqual(
			initialState
		)
	})

	it('removes transition', () => {
		expect(
			reducer(
				{
					...initialState,
					transitions: {
						...initialState.transitions,
						1: {
							id: '1',
							name: 'x',
							startState: 's1',
							endState: 's2'
						}
					}
				},
				Actions.removeTransition('1')
			)
		).toEqual({
			...initialState,
			transitions: {}
		} as State)
	})

	it('removes transition wrong id', () => {
		expect(reducer(initialState, Actions.removeTransition('1'))).toEqual(
			initialState
		)
	})

	it('toggles initial state', () => {
		expect(reducer(initialState, Actions.toggleInitialState('1'))).toEqual({
			...initialState,
			initialStates: ['1']
		} as State)
	})

	it('toggles initial state exists', () => {
		expect(
			reducer(
				{
					...initialState,
					initialStates: ['1']
				},
				Actions.toggleInitialState('1')
			)
		).toEqual({
			...initialState,
			initialStates: []
		} as State)
	})

	it('toggles final state', () => {
		expect(reducer(initialState, Actions.toggleFinalState('1'))).toEqual({
			...initialState,
			finalStates: ['1']
		} as State)
	})

	it('toggles final state exists', () => {
		expect(
			reducer(
				{
					...initialState,
					finalStates: ['1']
				},
				Actions.toggleFinalState('1')
			)
		).toEqual({
			...initialState,
			finalStates: []
		} as State)
	})

	it('sets data', () => {
		expect(reducer(initialState, Actions.set(initialState))).toEqual(
			initialState
		)
	})

	it('positions data', () => {
		const positionedLayer = layered(initialState)
		const forceLayer = force(initialState)
		expect(reducer(initialState, Actions.position('layer'))).toEqual(
			positionedLayer
		)
		expect(reducer(initialState, Actions.position('force'))).toEqual(
			forceLayer
		)
	})

	it('deletes data', () => {
		expect(reducer(initialState, Actions.delete())).toEqual(initialState)
	})
})
