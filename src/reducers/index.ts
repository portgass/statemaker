import { combineReducers } from 'redux'
import undoable, { StateWithHistory } from 'redux-undo'

import { ActionsUnion } from '../utils/redux'

import canvasReducer, {
	Actions as CanvasActions,
	State as CanvasState,
	initialState as initialCanvasState
} from './canvas'
import dataReducer, {
	Actions as DataActions,
	State as DataState,
	initialState as initialDataState
} from './data'
import toolbarReducer, {
	Actions as ToolbarActions,
	State as ToolbarState,
	initialState as initialToolabrState
} from './toolbar'

export const Actions = { ...CanvasActions, ...DataActions, ...ToolbarActions }
export type Actions = ActionsUnion<typeof Actions>

export interface State {
	canvas: CanvasState
	data: StateWithHistory<DataState>
	toolbar: ToolbarState
}

export const initialState: State = {
	canvas: initialCanvasState,
	data: {
		past: [],
		present: initialDataState,
		future: [],
		_latestUnfiltered: initialDataState,
		group: {},
		index: 1,
		limit: 10
	},
	toolbar: initialToolabrState
}

/** Main app reducer */
const reducer = combineReducers<State>({
	canvas: canvasReducer,
	data: undoable(dataReducer),
	toolbar: toolbarReducer
})

export default reducer
