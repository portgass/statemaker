import * as React from 'react'
import * as ReactDOM from 'react-dom'
import { Provider } from 'react-redux'

import App from './components/App'
import * as serviceWorker from './serviceWorker'
import store from './store'
import globalCursor from './utils/globalCursor'

/** Mount point */
ReactDOM.render(
	<Provider store={store}>
		<App />
	</Provider>,
	document.getElementById('root') as HTMLElement
)
serviceWorker.register()
globalCursor()
