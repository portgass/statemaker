import * as React from 'react'
import { useActions, useSelector } from 'react-redux'

import PositionIcon from '@material-ui/icons/GpsFixed'

import { Actions, State as ReduxState } from '../../reducers'
import ElementButton from './ElementButton'

/**
 * Automatically positions state machine
 *
 * @returns
 */
function PositionButton() {
	const [autoPosition] = useActions([Actions.position], [])
	const { algorithm } = useSelector(
		(state: ReduxState) => ({
			algorithm: state.toolbar.algorithm
		}),
		[]
	)

	const handlePosition = React.useCallback(() => {
		autoPosition(algorithm)
	}, [autoPosition, algorithm])

	return (
		<ElementButton
			Icon={PositionIcon}
			label="Auto-position"
			onAction={handlePosition}
		/>
	)
}

export default PositionButton
