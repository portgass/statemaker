import * as React from 'react'
import { useActions, useSelector } from 'react-redux'

import SaveIcon from '@material-ui/icons/Save'

import { State as ReduxState, Actions } from '../../reducers'
import { saveState } from '../../utils/persist'
import ElementButton from './ElementButton'

/**
 * Saves current state machine to browser local storage
 *
 * @returns
 */
function SaveButton() {
	const [setConfirmText] = useActions([Actions.setConfirmText], [])
	const { data } = useSelector(
		(state: ReduxState) => ({
			data: state.data.present
		}),
		[]
	)

	const handleSave = React.useCallback(() => {
		saveState(data)
		setConfirmText('Automata saved')
	}, [setConfirmText, data])

	return (
		<ElementButton
			Icon={SaveIcon}
			label="Save to memory"
			onAction={handleSave}
		/>
	)
}

export default SaveButton
