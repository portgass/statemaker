import * as React from 'react'
import { batch, useActions, useSelector } from 'react-redux'

import TransitionIcon from '@material-ui/icons/ArrowRightAlt'

import { Actions, State as ReduxState } from '../../reducers'
import ElementButton from './ElementButton'

/**
 * Allows building transition
 *
 * @returns
 */
function TransitionButton() {
	const [setCursorMode, toggleBuildTransition] = useActions(
		[Actions.setCursorMode, Actions.toggleBuildTransition],
		[]
	)
	const { selected } = useSelector(
		(state: ReduxState) => ({
			selected: state.toolbar.buildTransition
		}),
		[]
	)

	const handleToggleTrainsition = React.useCallback(() => {
		batch(() => {
			setCursorMode('select')
			toggleBuildTransition()
		})
	}, [setCursorMode, toggleBuildTransition])

	return (
		<ElementButton
			Icon={TransitionIcon}
			label="Add Transition"
			selected={selected}
			onAction={handleToggleTrainsition}
		/>
	)
}

export default TransitionButton
