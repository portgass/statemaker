import * as React from 'react'
import { useActions, useSelector } from 'react-redux'

import AlgorithmIcon from '@material-ui/icons/Settings'

import { Actions, State as ReduxState } from '../../reducers'
import MenuButton from './MenuButton'

/**
 * Sets positioning algorithm
 *
 * @returns
 */
function SetAlgorithmButton() {
	const [setAlgorithm] = useActions([Actions.setAlgorithm], [])
	const { algorithm } = useSelector(
		(state: ReduxState) => ({
			algorithm: state.toolbar.algorithm
		}),
		[]
	)

	const handleChooseAlg = React.useCallback(
		(variant: 'force' | 'layer') => () => {
			setAlgorithm(variant)
		},
		[setAlgorithm]
	)

	return (
		<React.Fragment>
			<MenuButton
				Icon={AlgorithmIcon}
				label="Choose positioning algorithm"
				selected={false}
				menuItems={[
					{
						label: 'Force-directed',
						selected: algorithm === 'force',
						onAction: handleChooseAlg('force')
					},
					{
						label: 'Hierarchical',
						selected: algorithm === 'layer',
						onAction: handleChooseAlg('layer')
					}
				]}
			/>
		</React.Fragment>
	)
}

export default SetAlgorithmButton
