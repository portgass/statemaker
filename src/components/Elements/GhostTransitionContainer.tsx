import * as React from 'react'
import { useSelector } from 'react-redux'

import usePosition from '../../hooks/usePosition'
import { State as ReduxState } from '../../reducers'
import { getEdgePointsCursor } from '../../utils/positioning'
import Transition from './Transition'

/**
 * Ghost transition shown when drawing new transition on canvas
 * Uses mouse cursor for position
 *
 * @returns
 */
function GhostTransitionContainer() {
	const { data, startState, buildTransition } = useSelector(
		(state: ReduxState) => {
			const transition = state.canvas.ghostTransition
			// Find the correct start state
			const startState = transition
				? state.data.present.states[transition.startState]
				: undefined

			return {
				data: {
					...transition,
					name: transition.name + state.canvas.transitionNameId
				},
				startState,
				buildTransition: state.toolbar.buildTransition
			}
		},
		[]
	)

	const position = usePosition(buildTransition)
	if (!startState || !buildTransition) return null

	// Calculate the start and end points of the transition
	const points = getEdgePointsCursor(startState, position)

	return <Transition data={data} {...points} isGhost={true} offset={0} />
}

export default GhostTransitionContainer
