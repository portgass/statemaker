import * as React from 'react'
import { batch, useActions, useSelector } from 'react-redux'

import useDrag from '../../hooks/useDrag'
import usePosition from '../../hooks/usePosition'
import useTextSize from '../../hooks/useTextSize'
import IState from '../../interfaces/State'
import { hasStateError } from '../../utils/errors'

import { Actions, State as ReduxState } from '../../reducers'
import State from './State'

interface Props {
	/** State id used to get state data */
	id: string
}

/**
 * Selects attributes from state
 *
 * @param {string} id Selected state's id
 */
const stateSelector = (id: string) => (state: ReduxState) => {
	let hasTransitionSelected = false

	// If there's selected transition
	if (!!state.canvas.selectedTransition) {
		const t =
			state.data.present.transitions[state.canvas.selectedTransition]
		// Mark if selected tansition leads from or to this state
		if (t) {
			hasTransitionSelected = t.startState === id || t.endState === id
		}
	}

	return {
		data: state.data.present.states[id],
		isSelected: id === state.canvas.selectedState,
		isInitial: state.data.present.initialStates.includes(id),
		isFinal: state.data.present.finalStates.includes(id),
		hasTransitionSelected,
		hasError: hasStateError(
			state.data.present.states,
			state.data.present.states[id]
		),
		buildTransition: state.toolbar.buildTransition,
		selectedTransition: state.canvas.selectedTransition,
		ghostTransition: state.canvas.ghostTransition,
		transitionNameId: state.canvas.transitionNameId,
		cursorMode: state.canvas.cursorMode
	}
}

/**
 * Represents state machine's individual state
 * It can be dragged around and be selected
 *
 * @param {Props} props
 * @returns
 */
function StateContainer(props: Props) {
	const { id } = props
	const [isDragging, setDragging] = useDrag()
	// Indicates mouse moving in drag mode
	const [isMoving, setMoving] = React.useState(false)
	const actions = useActions(Actions, [])
	// Binds thi state to update action
	const handleUpdate = React.useCallback(
		(state: Partial<IState>) => actions.updateState(id, state),
		[actions, id]
	)
	const {
		data,
		isSelected,
		isInitial,
		isFinal,
		hasTransitionSelected,
		hasError,
		buildTransition,
		selectedTransition,
		ghostTransition,
		transitionNameId,
		cursorMode
	} = useSelector(stateSelector(props.id), [props.id])

	// When moving around in drag mode, use local position
	const useLocalMove = isDragging && isMoving
	const position = usePosition(useLocalMove)
	// Ref to state name
	const textRef = useTextSize(data.name, handleUpdate, false)

	// Disallow click on elements behind
	const handleClick = React.useCallback(
		(e: React.MouseEvent<SVGCircleElement>) => {
			e.nativeEvent.stopImmediatePropagation()
		},
		[]
	)

	// Dragging state
	const handleMouseMove = React.useCallback(
		(e: React.MouseEvent<SVGCircleElement>) => {
			e.preventDefault()
			// Allow only in select mode
			if (cursorMode !== 'select') return
			if (isDragging) {
				// Signal state is moving
				setMoving(true)
				actions.setPositioningState(id)
			}
		},
		[actions, id, cursorMode, isDragging]
	)

	// Drag on hold
	const handleMouseDown = React.useCallback(
		(e: React.MouseEvent<SVGCircleElement>) => {
			e.preventDefault()
			// Allow only in select mode
			if (cursorMode !== 'select') return
			e.nativeEvent.stopImmediatePropagation()
			// Signal drag if transition isn't being manipulated
			if (selectedTransition || buildTransition) return
			setDragging(true)
		},
		[setDragging, cursorMode, selectedTransition, buildTransition]
	)

	// Handle mouse release on state
	const handleMouseUp = React.useCallback(
		(e: React.MouseEvent<SVGCircleElement>) => {
			// Do nothing if not in select mode
			if (cursorMode !== 'select') return
			// If transition is being built
			if (buildTransition) {
				// There's alrady start state
				if (ghostTransition.startState) {
					// Creaate transition
					batch(() => {
						actions.addTransition({
							...ghostTransition,
							name: ghostTransition.name + transitionNameId,
							endState: id
						})

						actions.useTransitionNameId()
						actions.updateGhostTransition({ startState: '' })

						actions.setBuildTransition(false)
					})
				} else {
					// No start stat ein ghost transition
					actions.updateGhostTransition({ startState: id })
				}
			} else if (isMoving) {
				// State is in drag mode
				// Stop dragging and set position
				setDragging(false)
				setMoving(false)
				batch(() => {
					actions.setPositioningState('')
					actions.updateState(id, { position })
				})
			} else {
				// Select state
				batch(() => {
					actions.selectState(id)
					actions.setInput(data.name)
				})
			}
		},
		[
			id,
			data.name,
			actions,
			cursorMode,
			isMoving,
			setDragging,
			buildTransition,
			ghostTransition,
			transitionNameId,
			position
		]
	)

	return (
		<State
			ref={textRef}
			data={useLocalMove ? { ...data, position } : data}
			isInitial={isInitial}
			isFinal={isFinal}
			isSelected={isSelected}
			hasError={hasError}
			hasTransitionSelected={hasTransitionSelected}
			buildTransition={buildTransition}
			showOverlay={
				cursorMode === 'select' &&
				(!buildTransition || ghostTransition.startState === id) &&
				!isMoving
			}
			onMouseDown={handleMouseDown}
			onMouseUp={handleMouseUp}
			onMouseMove={handleMouseMove}
			onClick={handleClick}
		/>
	)
}

export default StateContainer
