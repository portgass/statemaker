import * as React from 'react'

import { Theme } from '@material-ui/core/styles'
import { makeStyles } from '@material-ui/styles'

import IState from '../../interfaces/State'
import FinalState from './FinalState'
import InitialState from './InitialState'
import StateOverlay from './StateOverlay'

interface Props {
	/** State element data */
	data: IState
	isInitial?: boolean
	isFinal?: boolean
	isSelected?: boolean
	isGhost?: boolean
	/** Show tool overlay */
	showOverlay?: boolean
	hasError?: boolean
	/** Some transition from or to this state is selected */
	hasTransitionSelected?: boolean
	/** New transition from this state is being built */
	buildTransition?: boolean
	onClick?: (e: React.MouseEvent<SVGCircleElement, MouseEvent>) => void
	onMouseUp?: (e: React.MouseEvent<SVGCircleElement, MouseEvent>) => void
	onMouseDown?: (e: React.MouseEvent<SVGCircleElement, MouseEvent>) => void
	onMouseMove?: (e: React.MouseEvent<SVGCircleElement, MouseEvent>) => void
}

const noneString: 'none' = 'none'
const useStyles = makeStyles((theme: Theme) => ({
	root: {
		cursor: 'pointer',
		'&[data-ghost="true"]': {
			opacity: 0.4
		},
		'&:hover > #overlay': {
			display: 'block'
		},
		'& > #final': {
			'& > circle': {
				strokeWidth: 2,
				stroke: theme.palette.grey[400],
				fillOpacity: 0,
				rx: 4,
				ry: 4,
				pointerEvents: noneString
			}
		},
		'& > #initial': {
			'& > circle': {
				fill: theme.palette.grey[400]
			},
			'& > path': {
				strokeWidth: 2,
				stroke: theme.palette.grey[400]
			},
			'& > marker': {
				fill: theme.palette.grey[400]
			}
		}
	},
	state: {
		strokeWidth: 2,
		stroke: theme.palette.grey[400],
		fill: theme.palette.common.white,
		rx: 2,
		ry: 2,
		'&:hover': {
			stroke: theme.palette.primary.light
		},
		'&[data-error="true"]': {
			stroke: theme.palette.error.main
		},
		'&[data-selected="true"]': {
			stroke: theme.palette.primary.main
		},
		'&[data-transition-selected="true"]': {
			stroke: theme.palette.secondary.main
		}
	},
	text: {
		fill: theme.palette.text.primary,
		fontFamily: 'Roboto',
		fontSize: 20,
		pointerEvents: noneString
	}
}))

/**
 * Represents the state element in state machine
 *
 * @param {Props} props
 * @param {React.Ref<SVGTextElement>} textRef Reference to the name of state used to calculate the state size
 * @returns
 */
function State(props: Props, textRef: React.Ref<SVGTextElement>) {
	const {
		data,
		hasError,
		hasTransitionSelected,
		isInitial,
		isFinal,
		isGhost,
		isSelected,
		showOverlay,
		onClick,
		onMouseDown,
		onMouseMove,
		onMouseUp
	} = props
	const classes = useStyles()

	const id = 'state_' + data.id

	// Split name to array of lines
	const lines = data.name.split('\n')
	// Calculates the y position of each line, so the lines are not showing over each other
	const textYPosition = React.useCallback(
		(i: number) => i * 20 - ((lines.length - 1) * 20) / 2 + 6,
		[lines]
	)
	// Put each line in separate tspan element
	const mapLine = React.useCallback(
		(line: string, i: number) => (
			<tspan key={i} x={0} y={textYPosition(i) + 'px'}>
				{line}
			</tspan>
		),
		[textYPosition]
	)

	return (
		<g id={id} className={classes.root} data-ghost={isGhost}>
			<InitialState
				id={data.id}
				show={isInitial || false}
				position={data.position}
				size={data.size}
			/>
			<FinalState
				show={isFinal || false}
				position={data.position}
				size={data.size}
			/>
			<circle
				className={classes.state}
				r={data.size}
				cx={data.position.x}
				cy={data.position.y}
				onClick={onClick}
				onMouseUp={onMouseUp}
				onMouseDown={onMouseDown}
				onMouseMove={onMouseMove}
				data-error={hasError}
				data-selected={isSelected}
				data-transition-selected={hasTransitionSelected}
			/>
			<text
				className={classes.text}
				x={data.position.x}
				y={data.position.y}
				ref={textRef}
				alignmentBaseline="middle"
				textAnchor="middle"
				transform={`translate(${data.position.x},${data.position.y})`}
			>
				{lines.map(mapLine)}
			</text>
			<StateOverlay
				id={data.id}
				position={data.position}
				size={data.size}
				show={showOverlay || false}
			/>
		</g>
	)
}

export default React.memo(React.forwardRef(State), (prevProps, nextProps) => {
	if (prevProps.data.name !== nextProps.data.name) {
		return false
	}
	if (prevProps.data.position.x !== nextProps.data.position.x) return false
	if (prevProps.data.position.y !== nextProps.data.position.y) return false
	if (prevProps.data.size !== nextProps.data.size) return false
	if (prevProps.isInitial !== nextProps.isInitial) return false
	if (prevProps.isFinal !== nextProps.isFinal) return false
	if (prevProps.isSelected !== nextProps.isSelected) return false
	if (prevProps.isGhost !== nextProps.isGhost) return false
	if (prevProps.hasError !== nextProps.hasError) return false
	if (prevProps.showOverlay !== nextProps.showOverlay) return false
	if (prevProps.onClick !== nextProps.onClick) return false
	if (prevProps.onMouseDown !== nextProps.onMouseDown) return false
	if (prevProps.onMouseMove !== nextProps.onMouseMove) return false
	if (prevProps.onMouseUp !== nextProps.onMouseUp) return false
	if (prevProps.hasTransitionSelected !== nextProps.hasTransitionSelected)
		return false
	if (prevProps.buildTransition !== nextProps.hasTransitionSelected)
		return false
	return true
})
