import * as React from 'react'
import { useActions, useSelector } from 'react-redux'

import { makeStyles } from '@material-ui/styles'
import Snackbar from '@material-ui/core/Snackbar'
import SnackbarContent from '@material-ui/core/SnackbarContent'
import IconButton from '@material-ui/core/IconButton'
import CloseIcon from '@material-ui/icons/Close'

import { State as ReduxState, Actions } from '../reducers'

const useStyles = makeStyles({
	close: {
		padding: 4
	}
})

/**
 * Shows info snackbar
 *
 * @returns
 */
function Confirm() {
	const [open, setOpen] = React.useState(false)
	const classes = useStyles()
	const actions = useActions(Actions, [])
	const { text } = useSelector(
		(state: ReduxState) => ({
			text: state.toolbar.confirmText
		}),
		[]
	)

	// Close on close button click or when timeout
	const handleClose = React.useCallback(
		(_: React.MouseEvent<HTMLElement>, reason?: string) => {
			if (reason === 'clickaway') return
			setOpen(false)
			setTimeout(() => {
				actions.setConfirmText('')
			}, 1000)
		},
		[actions]
	)

	React.useEffect(() => {
		if (!!text) setOpen(true)
	}, [text])

	return (
		<Snackbar
			anchorOrigin={{
				vertical: 'top',
				horizontal: 'center'
			}}
			open={open}
			onClose={handleClose as any}
			autoHideDuration={1000}
		>
			<SnackbarContent
				message={text}
				action={
					<IconButton
						color="inherit"
						className={classes.close}
						onClick={handleClose}
					>
						<CloseIcon />
					</IconButton>
				}
			/>
		</Snackbar>
	)
}

export default Confirm
