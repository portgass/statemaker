import { createStore } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'

import reducer from './reducers'

/** Initialize app state store */
const store = createStore(reducer, composeWithDevTools())

export default store
