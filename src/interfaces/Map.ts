/**
 * Map of generic elements
 *
 * @interface Map
 * @template T
 */
interface Map<T> {
	[key: string]: T
}

export default Map
