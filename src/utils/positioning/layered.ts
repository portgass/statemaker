import uuid from 'uuid'

import Map from '../../interfaces/Map'
import TGraph, {
	Edge as TEdge,
	Vertex as TVertex,
	VertexMap as TVertexMap,
	VertexReducer as TVertexReducer,
	EdgeReducer as TEdgeReducer,
	toGraph,
	baseVertex,
	baseEdge,
	createElement,
	updateElement,
	toMap,
	toData,
	toArray
} from '../../interfaces/Graph'
import { PositionFn } from '../../interfaces/Transformations'
import { getTextSize } from '../textSize'

//-- TYPES

interface Vertex extends TVertex {
	layer: number
	size: number
	priority: number
	isInitial: boolean
	isVisited: boolean
	isMarked: boolean
	isTemp: boolean
}

interface Edge extends TEdge {
	makesCycle: boolean
}

type VertexMap = TVertexMap<Vertex>
interface Graph extends TGraph<Vertex, Edge> {
	layers: string[][]
}
type VertexReducer<T> = TVertexReducer<T, Vertex>
type EdgeReducer<T> = TEdgeReducer<T, Edge>
type MedianMap = Map<number>
type GraphTransform = (graph: Graph) => Graph

//-- CONSTS

const MIDDLE = (document.body.clientWidth || 800) / 2
const GAP_X = 20
const GAP_Y = 60
const STATE_SIZE = 20
const DEFAULT_V: Vertex = {
	...baseVertex,
	layer: 0,
	size: STATE_SIZE,
	priority: 0,
	isInitial: false,
	isMarked: false,
	isVisited: false,
	isTemp: false
}

const DEFAULT_E: Edge = {
	...baseEdge,
	makesCycle: false
}

//-- HELPERS

const createVertex = createElement(DEFAULT_V)
const createEdge = createElement(DEFAULT_E)

const addLayers = (graph: TGraph<Vertex, Edge>): Graph => ({
	...graph,
	layers: []
})

const setInitialVertices = (initialIds: string[]): GraphTransform => graph => ({
	...graph,
	vertices: {
		...graph.vertices,
		...initialIds.reduce<VertexMap>(
			(acc, curr) => ({
				...acc,
				[curr]: {
					...graph.vertices[curr],
					isInitial: true
				}
			}),
			{}
		)
	}
})

//-- MARK CYCLES

const markCycleEdge = (id: string): EdgeReducer<Edge[]> => (acc, curr) => [
	...acc,
	curr.id === id ? updateElement<Edge>({ makesCycle: true })(curr) : curr
]

const setEdgeMakesCycle = (edge: Edge): GraphTransform => graph => ({
	...graph,
	edges: graph.edges.reduce<Edge[]>(markCycleEdge(edge.id), [])
})

const dfsMarkEdges = (graph: Graph, vertex: Vertex): Graph => {
	if (vertex.isVisited) return graph
	graph.vertices[vertex.id].isVisited = true
	graph.vertices[vertex.id].isMarked = true

	const outEdges = graph.edges.filter(e => e.from === vertex.id)
	const newGraph = outEdges.reduce<Graph>(
		(acc, curr) =>
			acc.vertices[curr.to].isMarked
				? setEdgeMakesCycle(curr)(acc)
				: dfsMarkEdges(acc, acc.vertices[curr.to]),
		graph
	)
	newGraph.vertices[vertex.id].isMarked = false
	return newGraph
}

const markCycles: GraphTransform = graph =>
	toArray(graph.vertices)
		.sort((a, b) =>
			a.isInitial === b.isInitial ? 0 : a.isInitial ? -1 : 1
		)
		.reduce<Graph>(
			(acc, curr) =>
				!acc.vertices[curr.id].isVisited
					? dfsMarkEdges(acc, acc.vertices[curr.id])
					: acc,
			graph
		)

//-- MARK CYCLES

const getInitialStates = (edges: Edge[], vertices: string[]) =>
	vertices.filter(v => !edges.map(e => e.to).includes(v))

const topologicalSort: GraphTransform = graph => {
	let edges = graph.edges.reduce<Edge[]>(
		(acc, curr) => (curr.makesCycle ? acc : [...acc, curr]),
		[]
	)
	let vertices = Object.keys(graph.vertices)
	let initial = getInitialStates(edges, vertices)
	let layerIndex = 0

	while (initial.length > 0) {
		for (const v of initial) {
			const layer = graph.layers[layerIndex] || []
			graph.layers[layerIndex] = [...layer, v]
			graph.vertices[v].layer = layerIndex
		}

		// eslint-disable-next-line no-loop-func
		vertices = vertices.filter(v => !initial.includes(v))
		// eslint-disable-next-line no-loop-func
		edges = edges.filter(e => !initial.includes(e.from))
		initial = getInitialStates(edges, vertices)
		layerIndex++
	}

	return graph
}

const assignLayers: GraphTransform = graph => topologicalSort(graph)

//-- VERTEX ORDERING

const removeSpanningEdges: GraphTransform = graph => {
	graph.edges = graph.edges.reduce<Edge[]>((acc, curr) => {
		const fromState = graph.vertices[curr.from]
		const toState = graph.vertices[curr.to]

		const layerDiff = Math.abs(toState.layer - fromState.layer)
		if (layerDiff > 1) {
			const initLayer = Math.min(toState.layer, fromState.layer)
			let tempEdges: Edge[] = []
			let tempVertices: Vertex[] = [fromState, toState]
			for (const i of Array.from(Array(layerDiff).keys())) {
				if (i !== layerDiff - 1) {
					const id = uuid.v4()
					const layer = initLayer + i + 1
					const newVertex = createVertex({
						id,
						name: fromState.name + '->' + toState.name,
						layer,
						isTemp: true
					})
					tempVertices = [
						...tempVertices.slice(0, tempVertices.length - 1),
						newVertex,
						tempVertices[tempVertices.length - 1]
					]
					graph.layers[layer] = [...graph.layers[layer], newVertex.id]
				}

				const fromTemp = tempVertices[i]
				const toTemp = tempVertices[i + 1]

				const id = uuid.v4()
				tempEdges = [
					...tempEdges,
					createEdge({
						id,
						from: fromTemp.id,
						to: toTemp.id,
						name: fromTemp.name + '->' + toTemp.name
					})
				]
			}
			graph.vertices = {
				...graph.vertices,
				...tempVertices.reduce<VertexMap>(
					(acc, curr) => ({ ...acc, [curr.id]: curr }),
					{}
				)
			}
			return [...acc, ...tempEdges]
		} else {
			return [...acc, curr]
		}
	}, [])

	return graph
}

const getVertexPositions = (
	graph: Graph,
	vertex: string,
	layer: number
): number[] =>
	graph.edges
		.filter(e => e.to === vertex)
		.map(e => graph.vertices[e.from])
		.filter(v => v.layer === layer)
		.map(v => graph.layers[layer].indexOf(v.id))
		.sort((a, b) => a - b)

const getLayerPositions = (graph: Graph, layer: number) =>
	graph.edges
		.filter(e => graph.vertices[e.from].layer === layer)
		.map(e =>
			[graph.vertices[e.from], graph.vertices[e.to]].map(v =>
				graph.layers[v.layer].findIndex(id => id === v.id)
			)
		)

const countGraphCrossings = (graph: Graph) =>
	Array.from(Array(graph.layers.length).keys())
		.map(i => getLayerPositions(graph, i))
		.reduce(
			(count, layer) =>
				count +
				layer.reduce(
					(layerCount, pair) =>
						layerCount +
						layer.filter(
							tmpPair =>
								(pair[0] < tmpPair[0] &&
									pair[1] > tmpPair[1]) ||
								(pair[0] > tmpPair[0] && pair[1] < tmpPair[1])
						).length,
					0
				),
			0
		)

const assignMedian: GraphTransform = graph => {
	const medians = graph.layers.map((layer, i) =>
		i === 0
			? layer.reduce<MedianMap>(
					(acc, curr, id) => ({ ...acc, [curr]: id }),
					{}
			  )
			: layer.reduce<MedianMap>((acc, curr) => {
					const fromMedians = getVertexPositions(graph, curr, i - 1)
					const length = fromMedians.length
					const median = Math.floor((length + 1) / 2)
					const medianValue =
						length === 0 ? 0 : fromMedians[median - 1]
					return { ...acc, [curr]: medianValue }
			  }, {})
	)
	graph.layers = graph.layers.map((layer, i) =>
		layer.sort((a, b) => medians[i][a] - medians[i][b])
	)
	return graph
}

const countLayerCrossings = (left: number[], right: number[]): number =>
	left.reduce<number>(
		(acc, curr) => acc + right.filter(e => e < curr).length,
		0
	)

const countCrossings = (graph: Graph, left: string, right: string) => {
	const layer = graph.vertices[left].layer

	const positionsTopL = getVertexPositions(graph, left, layer - 1)
	const positionsBottomL = getVertexPositions(graph, left, layer + 1)

	const positionsTopR = getVertexPositions(graph, right, layer - 1)
	const positionsBottomR = getVertexPositions(graph, right, layer + 1)

	return (
		countLayerCrossings(positionsTopL, positionsTopR) +
		countLayerCrossings(positionsBottomL, positionsBottomR)
	)
}

const transposeLayer: GraphTransform = graph => {
	let improved = true

	while (improved) {
		improved = false
		// eslint-disable-next-line no-loop-func
		graph.layers = graph.layers.map((layer, index) => {
			for (let i = 0; i < layer.length - 2; i++) {
				const curr = layer[i]
				const next = layer[i + 1]
				if (
					countCrossings(graph, curr, next) >
					countCrossings(graph, next, curr)
				) {
					improved = true
					layer = [
						...layer.slice(0, i),
						next,
						curr,
						...layer.slice(i + 2, layer.length)
					]
				}
			}
			return layer
		})
	}

	return graph
}

const orderVertices: GraphTransform = graph => {
	graph = removeSpanningEdges(graph)
	for (let i = 0; i < 24; i++) {
		let newGraph = assignMedian(Object.assign({}, graph))
		newGraph = transposeLayer(newGraph)
		if (countGraphCrossings(graph) > countGraphCrossings(newGraph)) {
			graph = newGraph
		}
	}
	return graph
}

//-- POSITION ASSIGNMENT

const getLayerVertices = (graph: Graph) => (layer: string[]) =>
	layer.map(id => graph.vertices[id])

const assignVertexSize = (layer: Vertex[]) =>
	layer.map(v =>
		updateElement<Vertex>({
			size: v.isTemp ? STATE_SIZE : getTextSize(v.name)
		})(v)
	)

const getTempEdgesOfVertex = (graph: Graph) => (vertex: Vertex) =>
	graph.edges.filter(
		e =>
			(e.from === vertex.id && graph.vertices[e.to].isTemp) ||
			(e.to === vertex.id && graph.vertices[e.from].isTemp)
	)

const assignVertexPriority = (graph: Graph) => (layer: Vertex[]) =>
	layer.map(v =>
		updateElement<Vertex>({
			priority: getTempEdgesOfVertex(graph)(v).length + (v.isTemp ? 2 : 0)
		})(v)
	)

const getFromVerticesXPositions = (
	edges: Edge[],
	prevLayer: VertexMap,
	vertex: Vertex
) =>
	edges
		.filter(e => e.to === vertex.id)
		.map(e => prevLayer[e.from])
		.filter(v => v !== undefined)
		.map(v => v.position.x)

const getCenterOfPositions = (positions: number[]) =>
	positions.reduce((acc, curr) => acc + curr, 0) / positions.length

const getFirstPosition = (vertices: Vertex[], reverse: boolean = false) => {
	const arr = reverse ? vertices.slice().reverse() : vertices

	return arr.reduce<undefined | number>(
		(acc, curr) =>
			acc !== undefined
				? acc
				: curr.position.x === undefined
				? undefined
				: curr.position.x + curr.size * (reverse ? 1 : -1),
		undefined
	)
}

const addPosition = (position: number) => (vertex: Vertex) =>
	updateElement<Vertex>({
		position: {
			...vertex.position,
			x:
				vertex.position.x === undefined
					? undefined
					: vertex.position.x + position
		} as any
	})(vertex)

const getXPositionFromPrevLayer = (graph: Graph, prevLayer: VertexMap) => (
	acc: Vertex[],
	id: string
) => {
	const vertexId = acc.findIndex(v => v.id === id)
	const vertex = acc[vertexId]

	const fromPos = getFromVerticesXPositions(graph.edges, prevLayer, vertex)
	const center = getCenterOfPositions(fromPos)

	let prev = acc.slice(0, vertexId)
	let next = acc.slice(vertexId + 1, acc.length)
	const nearestLeft = getFirstPosition(prev, true)
	const nearestRight = getFirstPosition(next)
	const possible = vertex.size + GAP_X

	let position: number = 0
	if (nearestLeft && nearestRight) {
		if (
			center + possible < nearestRight &&
			center - possible > nearestLeft
		) {
			position = center
		} else {
			prev = prev.map(addPosition(-possible))
			next = next.map(addPosition(possible))
			position = (nearestLeft + nearestRight) / 2
		}
	} else if (nearestLeft) {
		if (center - possible > nearestLeft) {
			position = center
		} else {
			position = nearestLeft + possible
		}
	} else if (nearestRight) {
		if (center + possible < nearestRight) {
			position = center
		} else {
			position = nearestRight - possible
		}
	} else {
		position = center
	}

	return [
		...prev,
		updateElement<Vertex>({
			position: { ...vertex.position, x: position }
		})(vertex),
		...next
	]
}

const assignVertexXPosition = (graph: Graph) => (
	acc: Vertex[][],
	curr: Vertex[],
	index: number
) => {
	if (index === 0) {
		let cursor = 0
		const layer = curr.map(v => {
			v.position.x = cursor
			cursor += v.size * 2 + GAP_X
			return v
		})

		const offset = MIDDLE - (cursor - GAP_X) / 2
		return [
			layer.map(v =>
				updateElement<Vertex>({
					position: { x: v.position.x + offset, y: v.position.y }
				})(v)
			)
		]
	} else {
		const currSorted = curr
			.slice()
			.sort((a, b) => b.priority - a.priority)
			.map(v => v.id)

		return [
			...acc,
			currSorted.reduce<Vertex[]>(
				getXPositionFromPrevLayer(graph, toMap(acc[index - 1])),
				curr
			)
		]
	}
}

const assignPosition = (graph: Graph): Graph => {
	let verticalPosition = 100
	graph.vertices = graph.layers
		.map(getLayerVertices(graph))
		.map(assignVertexSize)
		.map(assignVertexPriority(graph))
		.reduce<Vertex[][]>(assignVertexXPosition(graph), [])
		.map((layer: Vertex[]) => {
			const newLayer = layer.map(vertex => {
				return updateElement<Vertex>({
					position: {
						x: vertex.position.x,
						y: verticalPosition
					}
				})(vertex)
			})
			const maxSize = layer.reduce<number>(
				(acc, curr) => (curr.size > acc ? curr.size : acc),
				0
			)
			verticalPosition += 2 * maxSize + GAP_Y
			return newLayer
		})
		.reduce<VertexMap>(
			(acc, curr) => ({
				...acc,
				...curr.reduce<VertexMap>((a, c) => ({ ...a, [c.id]: c }), {})
			}),
			{}
		)
	return graph
}

//-- MAIN

const position: PositionFn = data => {
	let graph = addLayers(toGraph<Vertex, Edge>(DEFAULT_V, DEFAULT_E)(data))
	graph = setInitialVertices(data.initialStates)(graph)
	graph = markCycles(graph)
	graph = assignLayers(graph)
	graph = orderVertices(graph)
	graph = assignPosition(graph)

	return toData(data, graph)
}

export default position
