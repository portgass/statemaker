import { mockRandom, resetMockRandom } from 'jest-mock-random'

import Data from '../../../interfaces/Data'
import force, { fk, fr, fa } from '../force'

const data: Data = {
	initialStates: [],
	finalStates: [],
	states: {
		1: {
			id: '1',
			name: '1',
			size: 0,
			position: { x: 0, y: 10 }
		},
		2: {
			id: '2',
			name: '2',
			size: 0,
			position: { x: 20, y: 30 }
		},
		3: {
			id: '3',
			name: '3',
			size: 0,
			position: { x: 40, y: 50 }
		},
		4: {
			id: '4',
			name: '4',
			size: 0,
			position: { x: 60, y: 70 }
		}
	},
	transitions: {
		5: {
			id: '5',
			name: '5',
			startState: '1',
			endState: '2'
		},
		6: {
			id: '6',
			name: '6',
			startState: '2',
			endState: '3'
		},
		7: {
			id: '7',
			name: '7',
			startState: '3',
			endState: '4'
		},
		8: {
			id: '8',
			name: '8',
			startState: '4',
			endState: '1'
		},
		9: {
			id: '9',
			name: '9',
			startState: '2',
			endState: '4'
		}
	}
}

const output: Data = {
	...data,
	states: {
		1: {
			...data.states[1],
			position: { x: 96.72, y: 251.72 }
		},
		2: {
			...data.states[2],
			position: { x: 232.05, y: 362.03 }
		},
		3: {
			...data.states[3],
			position: { x: 404.17, y: 329.08 }
		},
		4: {
			...data.states[4],
			position: { x: 268.13, y: 218.88 }
		}
	}
}

describe('force', () => {
	it('positions automata', () => {
		mockRandom([0.1, 0.2, 0.3, 0.2, 0.4, 0.1, 0.2, 0.3])
		expect(force(data)).toEqual(output)
		resetMockRandom()
	})
	it('positions empty automata', () => {
		expect(force({ ...data, states: {} })).toEqual({ ...data, states: {} })
	})

	it('fk handles zero', () => {
		expect(fk(200, 0)).toBe(0)
	})

	it('fa handles zero', () => {
		expect(fr(0, 10)).toBe(0)
	})

	it('fr handles zero', () => {
		expect(fa(10, 0)).toBe(0)
	})
})
