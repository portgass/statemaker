import Data from '../../../interfaces/Data'
import Position, { toPosition } from '../../../interfaces/Position'
import layer from '../layered'

const textSize = require('../../textSize')
textSize.getTextSize = jest.fn(() => 20)

const state = (id: number, position?: Position) => ({
	id: id.toString(),
	name: id.toString(),
	size: 20,
	position: position === undefined ? { x: 0, y: 0 } : position
})
const transition = (from: number, to: number) => ({
	id: from + '->' + to,
	name: from + '->' + to,
	startState: from.toString(),
	endState: to.toString()
})
const data: Data = {
	initialStates: ['10', '50'],
	finalStates: [],
	states: {
		10: state(10),
		20: state(20),
		30: state(30),
		40: state(40),
		50: state(50),
		60: state(60),
		70: state(70),
		80: state(80),
		90: state(90),
		100: state(100),
		101: state(101),
		102: state(102),
		103: state(103),
		104: state(104),
		105: state(105)
	},
	transitions: {
		103: transition(10, 20),
		104: transition(20, 10),
		200: transition(10, 30),
		300: transition(10, 40),
		400: transition(10, 100),
		500: transition(20, 50),
		600: transition(20, 60),
		700: transition(20, 102),
		800: transition(30, 60),
		900: transition(40, 70),
		1000: transition(40, 90),
		1001: transition(40, 102),
		1002: transition(50, 102),
		1003: transition(50, 80),
		1004: transition(60, 80),
		1005: transition(70, 101),
		1006: transition(70, 90),
		1007: transition(80, 100),
		1008: transition(90, 100),
		1009: transition(102, 100),
		10109: transition(70, 100),
		10139: transition(50, 103),
		10129: transition(80, 103),
		10128: transition(90, 104),
		10148: transition(90, 105)
	}
}

const output: Data = {
	...data,
	states: {
		10: state(10, toPosition(-180, 300)),
		20: state(20, toPosition(240, 200)),
		30: state(30, toPosition(315, 200)),
		40: state(40, toPosition(390, 200)),
		50: state(50, toPosition(-240, 300)),
		60: state(60, toPosition(-102.5, 300)),
		70: state(70, toPosition(-25, 300)),
		80: state(80, toPosition(-280, 400)),
		90: state(90, toPosition(377.5, 400)),
		100: state(100, toPosition(138.5, 500)),
		101: state(101, toPosition(-72.5, 400)),
		102: state(102, toPosition(-160, 400)),
		103: state(103, toPosition(-340, 500)),
		104: state(104, toPosition(377.5, 500)),
		105: state(105, toPosition(437.5, 500))
	}
}

describe('layered', () => {
	test('positions automata', () => {
		expect(layer(data)).toEqual(output)
	})
})
