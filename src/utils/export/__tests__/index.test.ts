import Data from '../../../interfaces/Data'
import Map from '../../../interfaces/Map'
import State from '../../../interfaces/State'
import Transition from '../../../interfaces/Transition'

import { downloadData } from '../'

jest.mock('file-saver')

type StateMap = Map<State>
type TransitionMap = Map<Transition>

const state1: State = {
	id: '1',
	name: 's1',
	position: { x: 0, y: 0 },
	size: 0
}
const state2: State = {
	id: '2',
	name: 's2',
	position: { x: 0, y: 0 },
	size: 0
}
const state3: State = {
	id: '3',
	name: 's3',
	position: { x: 0, y: 0 },
	size: 0
}
const state4: State = {
	id: '4',
	name: 's4',
	position: { x: 0, y: 0 },
	size: 0
}
const stateMap: StateMap = {
	1: state1,
	2: state2,
	3: state3,
	4: state4
}

const transition1: Transition = {
	id: '5',
	name: 't1',
	startState: '1',
	endState: '2'
}
const transition2: Transition = {
	id: '6',
	name: 't2',
	startState: '1',
	endState: '1'
}
const transition3: Transition = {
	id: '7',
	name: 't3',
	startState: '2',
	endState: '3'
}
const transition4: Transition = {
	id: '8',
	name: 't4',
	startState: '4',
	endState: '1'
}
const transitionMap: TransitionMap = {
	5: transition1,
	6: transition2,
	7: transition3,
	8: transition4
}

export const data: Data = {
	states: stateMap,
	transitions: transitionMap,
	initialStates: ['1'],
	finalStates: ['2']
}

test('expect download all file types', () => {
	downloadData(data, 'json')
	downloadData(data, 'json-xstate')
	downloadData(data, 'txt')
	downloadData(data, 'txt-fit')
	downloadData(data, '' as any)
})
