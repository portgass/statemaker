import { State as Canvas, initialState } from '../../reducers/canvas'
import Data from '../../interfaces/Data'
import Map from '../../interfaces/Map'
import State from '../../interfaces/State'
import Transition from '../../interfaces/Transition'
import { hasStateError, hasTransitionError, getErrors } from '../errors'

type StateMap = Map<State>
type TransitionMap = Map<Transition>

const state1: State = {
	id: '1',
	name: 's',
	position: { x: 0, y: 0 },
	size: 0
}
const state2: State = {
	id: '2',
	name: '',
	position: { x: 0, y: 0 },
	size: 0
}
const state3: State = {
	id: '3',
	name: 's',
	position: { x: 0, y: 0 },
	size: 0
}
const state4: State = {
	id: '3',
	name: 'd',
	position: { x: 0, y: 0 },
	size: 0
}
const stateMap: StateMap = {
	1: state1,
	2: state2,
	3: state3,
	4: state4
}

const transition1: Transition = {
	id: '1',
	name: 't',
	startState: 's1',
	endState: 's2'
}
const transition2: Transition = {
	id: '2',
	name: 't',
	startState: 's1',
	endState: 's2'
}
const transition3: Transition = {
	id: '3',
	name: '',
	startState: '',
	endState: ''
}
const transition4: Transition = {
	id: '4',
	name: 't2',
	startState: '',
	endState: ''
}
const transitionMap: TransitionMap = {
	1: transition1,
	2: transition2,
	3: transition3,
	4: transition4
}

const data: Data = {
	states: stateMap,
	transitions: transitionMap,
	initialStates: [],
	finalStates: []
}

const canvas: Canvas = {
	...initialState
}

test('undefined state has no error', () => {
	expect(hasStateError(stateMap, undefined)).toEqual(false)
})
test('unnamed state has error', () => {
	expect(hasStateError(stateMap, state2)).toEqual(true)
})
test('same name state has error', () => {
	expect(hasStateError(stateMap, state1)).toEqual(true)
})
test('uniq named state has no error', () => {
	expect(hasStateError(stateMap, state4)).toEqual(false)
})

test('undefined transition has no error', () => {
	expect(hasTransitionError(transitionMap, undefined)).toEqual(false)
})
test('unnamed transition has error', () => {
	expect(hasTransitionError(transitionMap, transition3)).toEqual(true)
})
test('same name transition has error', () => {
	expect(hasTransitionError(transitionMap, transition1)).toEqual(true)
})
test('uniq named transition has no error', () => {
	expect(hasTransitionError(transitionMap, transition4)).toEqual(false)
})

test('expect get errors', () => {
	expect(getErrors(data, canvas)).toEqual([
		'State has no name',
		'Transition has no name',
		'Multiple states with name: s',
		'Multiple transitions from state with name: t'
	])
})
