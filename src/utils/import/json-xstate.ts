import uuid from 'uuid'

import Data, { StateMap, TransitionMap } from '../../interfaces/Data'
import { ImportFn } from '../../interfaces/Transformations'

interface ExportData {
	id: string
	initial: string
	states: {
		[key: string]: {
			on: { [key: string]: string }
		}
	}
}

/**
 * Convert json-xstate data string to data object
 *
 * @param {string} dataString
 * @returns
 */
const importData: ImportFn = dataString => {
	const importData = JSON.parse(dataString) as ExportData
	const data: Data = {
		initialStates: [],
		finalStates: [],
		// Create states with default params
		states: Object.keys(importData.states).reduce(
			(acc, curr) => {
				const id = uuid.v4()
				return {
					...acc,
					[id]: {
						id,
						name: curr,
						position: { x: 0, y: 0 },
						size: 0
					}
				}
			},
			{} as StateMap
		),
		transitions: {}
	}

	// Find initial state
	const state = Object.values(data.states).find(
		s => s.name === importData.initial
	)
	data.initialStates = state ? [state.id] : []

	// Creates transitions
	data.transitions = Object.keys(importData.states).reduce(
		(acc, curr) => ({
			...acc,
			...Object.keys(importData.states[curr].on).reduce(
				(a, c) => {
					const id = uuid.v4()
					const startState = Object.values(data.states).find(
						s => s.name === curr
					)
					const endState = Object.values(data.states).find(
						s => s.name === importData.states[curr].on[c]
					)

					if (!startState || !endState) {
						return a
					}

					return {
						...a,
						[id]: {
							id,
							name: c,
							startState: startState.id,
							endState: endState.id
						}
					}
				},
				{} as TransitionMap
			)
		}),
		{} as TransitionMap
	)

	return data
}

export default importData
