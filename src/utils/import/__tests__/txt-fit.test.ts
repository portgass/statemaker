import importData from '../txt-fit'

import Data from '../../../interfaces/Data'
import Map from '../../../interfaces/Map'
import State from '../../../interfaces/State'
import Transition from '../../../interfaces/Transition'

type StateMap = Map<State>
type TransitionMap = Map<Transition>

const state1: State = {
	id: '1',
	name: 's1',
	position: { x: 0, y: 0 },
	size: 0
}
const state2: State = {
	id: '2',
	name: 's2',
	position: { x: 0, y: 0 },
	size: 0
}
const state3: State = {
	id: '3',
	name: 's3',
	position: { x: 0, y: 0 },
	size: 0
}
const state4: State = {
	id: '4',
	name: 's4',
	position: { x: 0, y: 0 },
	size: 0
}
const stateMap: StateMap = {
	1: state1,
	2: state2,
	3: state3,
	4: state4
}

const transition1: Transition = {
	id: '5',
	name: 't1',
	startState: '1',
	endState: '2'
}
const transition2: Transition = {
	id: '6',
	name: 't2',
	startState: '1',
	endState: '1'
}
const transition3: Transition = {
	id: '7',
	name: 't3',
	startState: '2',
	endState: '3'
}
const transition4: Transition = {
	id: '9',
	name: 't4',
	startState: '4',
	endState: '1'
}
const transition5: Transition = {
	id: '9',
	name: 't5',
	startState: '4',
	endState: '1a'
}
const transition6: Transition = {
	id: '10',
	name: 't4',
	startState: '4',
	endState: '2'
}
const transitionMap: TransitionMap = {
	5: transition1,
	6: transition2,
	7: transition3,
	9: transition4
}

export const data: Data = {
	states: stateMap,
	transitions: transitionMap,
	initialStates: ['1'],
	finalStates: ['1', '2']
}

export const file: string = `FA	t1	t2	t3	t4
><s1	s2	s1	-	-
<s2	-	-	s3	-
s3	-	-	-	-
s4	-	-	s5	s1`

describe('import txt-fit', () => {
	afterEach(() => {
		jest.resetModules()
	})
	beforeEach(() => {
		jest.mock('uuid')
	})

	it('converts', () => {
		expect(importData(file)).toEqual(data)
	})
})
