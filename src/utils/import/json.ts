import uuid from 'uuid'

import Data, { StateMap, TransitionMap } from '../../interfaces/Data'
import { ImportFn } from '../../interfaces/Transformations'

interface ExportData {
	initial: string[]
	final: string[]
	states: {
		[key: string]: {
			[key: string]: string
		}
	}
}

/**
 * Convert json data string to data object
 *
 * @param {string} dataString
 * @returns
 */
const importData: ImportFn = (dataString: string) => {
	const importData = JSON.parse(dataString) as ExportData
	const data: Data = {
		initialStates: [],
		finalStates: [],
		// Create states
		states: Object.keys(importData.states).reduce(
			(acc, curr) => {
				const id = uuid.v4()
				return {
					...acc,
					[id]: {
						id,
						name: curr,
						position: { x: 0, y: 0 },
						size: 0
					}
				}
			},
			{} as StateMap
		),
		transitions: {}
	}

	// Find initial states
	data.initialStates = importData.initial.reduce(
		(acc, curr) => {
			const id = Object.values(data.states).find(s => s.name === curr)
			return id ? [...acc, id.id] : acc
		},
		[] as string[]
	)

	// Find final states
	data.finalStates = importData.final.reduce(
		(acc, curr) => {
			const id = Object.values(data.states).find(s => s.name === curr)
			return id ? [...acc, id.id] : acc
		},
		[] as string[]
	)

	// Create transitions
	data.transitions = Object.keys(importData.states).reduce(
		(acc, curr) => ({
			...acc,
			...Object.keys(importData.states[curr]).reduce(
				(a, c) => {
					const id = uuid.v4()
					const startState = Object.values(data.states).find(
						s => s.name === curr
					)
					const endState = Object.values(data.states).find(
						s => s.name === importData.states[curr][c]
					)

					if (!startState || !endState) {
						return a
					}

					return {
						...a,
						[id]: {
							id,
							name: c,
							startState: startState.id,
							endState: endState.id
						}
					}
				},
				{} as TransitionMap
			)
		}),
		{} as TransitionMap
	)

	return data
}

export default importData
