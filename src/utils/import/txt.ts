import uuid from 'uuid'

import Data, { StateMap, TransitionMap } from '../../interfaces/Data'
import { ImportFn } from '../../interfaces/Transformations'

/**
 * Convert txt data string to data object
 *
 * @param {string} dataString
 * @returns
 */
const importData: ImportFn = (dataString: string) => {
	const importData = dataString.split('\n').map(r => r.split('\t'))
	const data: Data = {
		initialStates: [],
		finalStates: [],
		states: {},
		transitions: {}
	}

	// Create states
	data.states = importData[0].reduce(
		(acc, curr) => {
			const id = uuid.v4()
			return {
				...acc,
				[id]: {
					id,
					name: curr,
					position: { x: 0, y: 0 },
					size: 0
				}
			}
		},
		{} as StateMap
	)

	// Find initial states
	data.initialStates = importData[1].reduce(
		(acc, curr) => {
			const id = Object.values(data.states).find(s => s.name === curr)
			return id ? [...acc, id.id] : acc
		},
		[] as string[]
	)

	// Find final states
	data.finalStates = importData[2].reduce(
		(acc, curr) => {
			const id = Object.values(data.states).find(s => s.name === curr)
			return id ? [...acc, id.id] : acc
		},
		[] as string[]
	)

	// Create transitions
	// state1 -> transition -> state2
	data.transitions = importData.slice(3).reduce(
		(acc, curr) => {
			const startStateName = curr[0]
			const name = curr[1]
			const endStateName = curr[2]

			const id = uuid.v4()
			const startState = Object.values(data.states).find(
				s => s.name === startStateName
			)
			const endState = Object.values(data.states).find(
				s => s.name === endStateName
			)

			if (!startState || !endState) {
				return acc
			}

			return {
				...acc,
				[id]: {
					id,
					name,
					startState: startState.id,
					endState: endState.id
				}
			}
		},
		{} as TransitionMap
	)

	return data
}

export default importData
