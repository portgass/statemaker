import uuid from 'uuid'

import Data, { TransitionMap } from '../../interfaces/Data'
import { ImportFn } from '../../interfaces/Transformations'

interface ImportState {
	id: string
	name: string
	initial: boolean
	final: boolean
	transitions: {
		[key: string]: string
	}
}

/**
 * Convert txt-fit data string to data object
 *
 * @param {string} dataString
 * @returns
 */
const importData: ImportFn = (dataString: string) => {
	const importData = dataString.split('\n').map(r => r.split('\t'))
	const data: Data = {
		initialStates: [],
		finalStates: [],
		states: {},
		transitions: {}
	}
	// Ignore FA id and get transitions
	const transitionNames = importData[0].slice(1)

	// 				t1	t2			t3
	// [>][<]state 	- 	tostate 	-
	const states: ImportState[] = importData.slice(1).map(s => {
		const id = uuid.v4()
		let name: string = s[0]
		let initial: boolean = false
		let final: boolean = false

		// Initial id > is first char
		if (name.length > 0 && name[0] === '>') {
			initial = true
			name = name.slice(1)
		}
		// Final id < is next first char
		if (name.length > 0 && name[0] === '<') {
			final = true
			name = name.slice(1)
		}

		// Get transitions
		const transitions = s
			.slice(1)
			.reduce(
				(acc, curr, i) =>
					curr === '-' ? acc : { ...acc, [transitionNames[i]]: curr },
				{}
			)

		return {
			id,
			name,
			initial,
			final,
			transitions
		}
	})

	// Create objects
	data.states = states.reduce(
		(acc, curr) => ({
			...acc,
			[curr.id]: {
				id: curr.id,
				name: curr.name,
				position: { x: 0, y: 0 },
				size: 0
			}
		}),
		{}
	)

	data.initialStates = states.reduce(
		(acc, curr) => (curr.initial ? [...acc, curr.id] : acc),
		[] as string[]
	)

	data.finalStates = states.reduce(
		(acc, curr) => (curr.final ? [...acc, curr.id] : acc),
		[] as string[]
	)

	data.transitions = states.reduce(
		(acc, curr) => ({
			...acc,
			...Object.keys(curr.transitions).reduce((a, c) => {
				const id = uuid.v4()
				const endStateName = curr.transitions[c]
				const endState = Object.values(data.states).find(
					s => s.name === endStateName
				)

				if (!endState) {
					return a
				}

				return {
					...a,
					[id]: {
						id,
						name: c,
						startState: curr.id,
						endState: endState.id
					}
				}
			}, {})
		}),
		{} as TransitionMap
	)

	return data
}

export default importData
