import { getSizeFromRef } from './size'

/**
 * Creates temp element with text and gets its size
 *
 * @export
 * @param {string} text
 * @returns {number}
 */
export function getTextSize(text: string): number {
	const root = document.getElementById('canvas')
	if (!root) return 0

	const textEl = document.createElementNS(
		'http://www.w3.org/2000/svg',
		'text'
	)

	// Split name to array of lines
	const lines = text.split('\n')
	// Calculates the y position of each line, so the lines are not showing over each other
	const textYPosition = (i: number) =>
		i * 20 - ((lines.length - 1) * 20) / 2 + 6

	lines.forEach((line, i) => {
		const lineEl = document.createElementNS(
			'http://www.w3.org/2000/svg',
			'tspan'
		)
		lineEl.innerHTML = line
		lineEl.setAttributeNS(null, 'alignment-baseline', 'middle')
		lineEl.setAttributeNS(null, 'text-anchor', 'middle')
		lineEl.setAttributeNS(null, 'font-family', 'Roboto')
		lineEl.setAttributeNS(null, 'font-size', '20')
		lineEl.setAttributeNS(null, 'visibility', 'none')
		lineEl.setAttributeNS(null, 'x', '0')
		lineEl.setAttributeNS(null, 'y', textYPosition(i) + 'px')
		textEl.appendChild(lineEl)
	})

	root.appendChild(textEl)
	textEl.setAttributeNS(null, 'alignment-baseline', 'middle')
	textEl.setAttributeNS(null, 'text-anchor', 'middle')
	textEl.setAttributeNS(null, 'font-family', 'Roboto')
	textEl.setAttributeNS(null, 'font-size', '20')
	textEl.setAttributeNS(null, 'visibility', 'none')

	const size = getSizeFromRef(textEl)
	root.removeChild(textEl)

	return size
}
