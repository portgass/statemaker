import { State } from '../reducers/data'

const STORAGE_KEY = 'statemaker'
const STORAGE_KEY_EXPLORE = STORAGE_KEY + '_explore'
const VERSION = '0.2'

/**
 * Delete storage data
 *
 * @export
 */
export function deleteState() {
	localStorage.removeItem(STORAGE_KEY)
}

/**
 * Save data storage
 *
 * @export
 * @param {State} state
 */
export function saveState(state: State) {
	localStorage.setItem(
		STORAGE_KEY,
		JSON.stringify({ data: state, version: VERSION })
	)
}

/**
 * Load data from storage
 *
 * @export
 * @returns {(State | undefined)}
 */
export function loadState(): State | undefined {
	const storage = localStorage.getItem(STORAGE_KEY)
	if (storage) {
		// Convert to JSON
		const parsed: { data: State; version: string } = JSON.parse(storage)
		// Don't use if wrong version
		if (VERSION === parsed.version) return parsed.data
	}
}

/**
 * Get explore toggle value from atorage
 * If there is not any record pass true
 *
 * @export
 * @returns {boolean}
 */
export function getExploreValue(): boolean {
	const storage = localStorage.getItem(STORAGE_KEY_EXPLORE)
	if (storage) {
		const parsed: { show: boolean } = JSON.parse(storage)
		return parsed.show
	}
	return true
}

/**
 * Save explore value to storage
 * If there is none, create
 *
 * @export
 * @returns
 */
export function toggleExplore() {
	const show = !getExploreValue()
	localStorage.setItem(STORAGE_KEY_EXPLORE, JSON.stringify({ show }))
	return show
}
