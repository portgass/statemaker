import Data from '../../../interfaces/Data'
import Map from '../../../interfaces/Map'
import State from '../../../interfaces/State'
import Transition from '../../../interfaces/Transition'

import validate from '../data'

type StateMap = Map<State>
type TransitionMap = Map<Transition>

const state1: State = {
	id: '1',
	name: 's',
	position: { x: 0, y: 0 },
	size: 0
}
const state2: State = {
	id: '2',
	name: '',
	position: { x: 0, y: 0 },
	size: 0
}
const state3: State = {
	id: '3',
	name: 's',
	position: { x: 0, y: 0 },
	size: 0
}
const state4: State = {
	id: '3',
	name: 'd',
	position: { x: 0, y: 0 },
	size: 0
}
const stateMap: StateMap = {
	1: state1,
	2: state2,
	3: state3,
	4: state4
}

const transition1: Transition = {
	id: '1',
	name: 't',
	startState: '1',
	endState: '2'
}
const transition2: Transition = {
	id: '2',
	name: 't',
	startState: '1',
	endState: '2'
}
const transition3: Transition = {
	id: '3',
	name: '',
	startState: '7',
	endState: ''
}
const transition4: Transition = {
	id: '4',
	name: 't2',
	startState: '',
	endState: '8'
}
const transitionMap: TransitionMap = {
	1: transition1,
	2: transition2,
	3: transition3,
	4: transition4
}

const data: Data = {
	states: stateMap,
	transitions: transitionMap,
	initialStates: ['1', '8', ''],
	finalStates: ['2', '9', '']
}

test('expect get correct errors', () => {
	expect(validate(data)).toEqual([
		"Initial state doesn't exist - 8",
		"Initial state doesn't exist - ",
		"Final state doesn't exist - 9",
		"Final state doesn't exist - ",
		"Transition start state doesn't exist - 7",
		"Transition start state doesn't exist - ",
		"Transition end state doesn't exist - ",
		"Transition end state doesn't exist - 8",
		"Initial state can't be empty string",
		"Final state can't be empty string",
		"State name can't be empty string",
		"Transition name can't be empty string",
		"Transition start state name can't be empty string",
		"Transition end state name can't be empty string"
	])
})
