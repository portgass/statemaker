import validate from '../txt-fit'

const data: any[][] = [
	['FA', '1', '2', {}],
	['a', 'b', 'b', '-'],
	['b', 'b', '-'],
	['c', 2, 'b', '-']
]

test('expect error with file 0 lines', () => {
	expect(validate([])).toEqual(['Missing data lines'])
})

test('expect error with file id not FA', () => {
	expect(validate([[]])).toEqual(['Data identifier type is not FA'])
})

test('expect get correct errors', () => {
	expect(validate(data)).toEqual([
		'[object Object] is not string or number',
		'State 1: length error (not 4)',
		'State 2: type error (not string)'
	])
})
