import { existsInArr, hasEmptyString } from '../validation'
import Data from '../../interfaces/Data'
import { ValidateFn } from '../../interfaces/Transformations'

/**
 * Validate data to check for state machine constraints
 *
 * @param {*} data
 * @returns
 */
const validate: ValidateFn<Data> = data => {
	const stateIds = Object.keys(data.states)
	const states = Object.values(data.states)
	const transitions = Object.values(data.transitions)

	const errors = [
		...existsInArr(data.initialStates, 'Initial state')(stateIds),
		...existsInArr(data.finalStates, 'Final state')(stateIds),
		...existsInArr(
			transitions.map(s => s.startState),
			'Transition start state'
		)(stateIds),
		...existsInArr(
			transitions.map(s => s.endState),
			'Transition end state'
		)(stateIds),
		...hasEmptyString(data.initialStates, 'Initial state'),
		...hasEmptyString(data.finalStates, 'Final state'),
		...hasEmptyString(states.map(s => s.name), 'State name'),
		...hasEmptyString(transitions.map(s => s.name), 'Transition name'),
		...hasEmptyString(
			transitions.map(s => s.startState),
			'Transition start state name'
		),
		...hasEmptyString(
			transitions.map(s => s.endState),
			'Transition end state name'
		)
	]

	return errors
}

export default validate
