import Position, {
	round,
	add,
	subtract,
	divideByX,
	multiplyByX,
	toPosition
} from '../interfaces/Position'
import State from '../interfaces/State'

const MIN_STATE_SIZE = 20
const BORDER_OFFSET = 4

/**
 * Get angle between points (start.x, start.y) and (end.x, end.y)
 *
 * @param {Position} start
 * @param {Position} end
 */
export const getAngle = (start: Position, end: Position) =>
	Math.atan2(start.y - end.y, start.x - end.x)
/**
 * Get distance between two points
 *
 * @param {Position} start
 * @param {Position} end
 */
export const getDistance = (start: Position, end: Position) =>
	Math.hypot(end.x - start.x, end.y - start.y)

/**
 * Find the middle position between two positions
 *
 * @param {Position} start
 * @param {Position} end
 * @returns {Position}
 */
export const middlePoint = (start: Position, end: Position): Position =>
	add(start, divideByX(subtract(end, start), 2))

/**
 * Get position on line between start and end positions
 * Position is offset <0, 1>
 *
 * @param {Position} start
 * @param {Position} end
 * @param {number} offset
 * @returns {Position}
 */
export const linePoint = (
	start: Position,
	end: Position,
	offset: number
): Position => {
	const angle = getAngle(start, end)
	const distance = getDistance(start, end)
	const degrees = toPosition(Math.cos(angle), Math.sin(angle))

	return subtract(start, multiplyByX(degrees, distance * offset))
}

/**
 * Get position on a perpendicular line to line between start and end positions from position point
 *  Position is offset by offset * MIN_STATE_SIZE
 *
 * @param {Position} start
 * @param {Position} end
 * @param {Position} point
 * @param {number} offset
 * @returns {Position}
 */
export const curvePoint = (
	start: Position,
	end: Position,
	point: Position,
	offset: number
): Position => {
	const angle = getAngle(start, end)
	const degrees = toPosition(-Math.sin(angle), Math.cos(angle))

	return add(point, multiplyByX(degrees, offset * MIN_STATE_SIZE))
}

/**
 * Gets distance between the center of circle to the edge on an angle
 *
 * @param {number} size
 * @param {number} angle
 * @returns {Position}
 */
export const edgeOffset = (size: number, angle: number): Position => {
	const actualSize = Math.max(size, MIN_STATE_SIZE)
	const degrees = toPosition(Math.cos(angle), Math.sin(angle))
	return multiplyByX(degrees, actualSize + BORDER_OFFSET)
}

interface EdgePoints {
	fromPoint: Position
	toPoint: Position
}

/**
 * Gets points on start and end states that start on their edges and are on line with shortest distance possible
 *
 * @param {State} startState
 * @param {State} endState
 * @returns {EdgePoints}
 */
export const getEdgePoints = (
	startState: State,
	endState: State
): EdgePoints => {
	const startPoint = startState.position
	const endPoint = endState.position

	const angle = getAngle(startPoint, endPoint)

	// Change the points from center to the edges
	const from = subtract(startPoint, edgeOffset(startState.size, angle))
	const to = add(endPoint, edgeOffset(endState.size, angle))

	return {
		fromPoint: round(from),
		toPoint: round(to)
	}
}

/**
 * Gets points on start state and cursor position that start on state edge and are on line with shortest distance possible
 *
 * @param {State} startState
 * @param {Position} cursor
 * @returns {EdgePoints}
 */
export const getEdgePointsCursor = (
	state: State,
	cursor: Position
): EdgePoints => {
	const startPoint = state.position

	const angle = getAngle(startPoint, cursor)

	return {
		fromPoint: round(subtract(startPoint, edgeOffset(state.size, angle))),
		toPoint: round(cursor)
	}
}
