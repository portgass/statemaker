import { toPosition, distance } from '../interfaces/Position'

const MIN_STATE_SIZE = 20
const BORDER_OFFSET = 4

/**
 * Gets the radius size of box
 *
 * @param {SVGTextElement} el
 * @returns {number}
 */
export const getSizeFromRef = (el: SVGTextElement): number => {
	if (!el) return MIN_STATE_SIZE

	const bounds = el.getBBox()
	let size = distance(toPosition(bounds.width, bounds.height))
	size /= 2 // Get radius
	size = Math.max(MIN_STATE_SIZE, size)
	size += BORDER_OFFSET

	return size
}
