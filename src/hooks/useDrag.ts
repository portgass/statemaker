import * as React from 'react'

/**
 * Listens on global mouse up and disables drag when triggered
 *
 * @returns {[ boolean, React.Dispatch<React.SetStateAction<boolean>> ]}
 */
const useDrag = (): [
	boolean,
	React.Dispatch<React.SetStateAction<boolean>>
] => {
	const [selected, setSelected] = React.useState(false)

	const handleDeselect = React.useCallback(() => {
		setSelected(false)
	}, [])

	React.useLayoutEffect(() => {
		// Do nothing if no state selected
		if (!selected) return

		document.addEventListener('mouseup', handleDeselect)
		return () => {
			document.removeEventListener('mouseup', handleDeselect)
		}
	}, [handleDeselect, selected])

	return [selected, setSelected]
}

export default useDrag
